﻿public class LetterRepositoryLocal : ILetterRepository
{
    public Letter GetByLetter(char letter)
    {
        return new Letter(letter);
    }
}