﻿public interface IRoundRepository
{
    Round GetById(int roundId, int gameId);
    void Save(Round round);
}

