﻿public interface ICategoryRepository
{
    Category GetByType(GameEnum.GAME_CATEGORIES categoryType);
}