﻿using UnityEngine;

public class PlayerRepositoryBinaryFormatter : IPlayerRepository
{
    public void Save(Player player)
    {
        string path = Application.persistentDataPath + "/player" + player.Name + ".bin";
        RepositoryBinaryFormatter<Player>.Save(player, path);
    }

    public Player GetByName(string name)
    {
        string path = Application.persistentDataPath + "/player" + name + ".bin";

        return RepositoryBinaryFormatter<Player>.ReadFromFilePath(path);
    }

    public Player GetLocalPlayer()
    {
        string path = Application.persistentDataPath + "/player.bin";

        return RepositoryBinaryFormatter<Player>.ReadFromFilePath(path);
    }

    public Player Get(string name, string password)
    {
        throw new System.NotImplementedException();
    }
}
