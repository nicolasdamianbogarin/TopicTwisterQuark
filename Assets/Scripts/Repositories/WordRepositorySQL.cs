﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;

public class WordRepositorySQL : MonoBehaviour, IWordRepository
{
    private const string _host = "https://localhost:7279/";
    private List<Word> result = new();
    private Dictionary<GameEnum.GAME_CATEGORIES, HashSet<string>> _validWords = new Dictionary<GameEnum.GAME_CATEGORIES, HashSet<string>>()
        {
            { GameEnum.GAME_CATEGORIES.Artists, new HashSet<string>() { "Da Vinci" } },
            { GameEnum.GAME_CATEGORIES.Sports, new HashSet<string>() { "Test" } },
            { GameEnum.GAME_CATEGORIES.Books, new HashSet<string>() { "Test" } },
            { GameEnum.GAME_CATEGORIES.Countries, new HashSet<string>() { "Test" } },
            { GameEnum.GAME_CATEGORIES.Movies, new HashSet<string>() { "Test" } },
            { GameEnum.GAME_CATEGORIES.Videogames, new HashSet<string>() { "Doom" } },
            { GameEnum.GAME_CATEGORIES.Sciencetists, new HashSet<string>() { "Test" } },
            { GameEnum.GAME_CATEGORIES.Random, new HashSet<string>() { "Test" } },
        };
    private Dictionary<GameEnum.GAME_CATEGORIES, string> _enumToString = new Dictionary<GameEnum.GAME_CATEGORIES, string>()
        {
            { GameEnum.GAME_CATEGORIES.Artists, "artists" },
            { GameEnum.GAME_CATEGORIES.Sports, "sports" },
            { GameEnum.GAME_CATEGORIES.Books, "books" },
            { GameEnum.GAME_CATEGORIES.Countries, "countries" },
            { GameEnum.GAME_CATEGORIES.Movies, "movies" },
            { GameEnum.GAME_CATEGORIES.Videogames, "videogames" },
            { GameEnum.GAME_CATEGORIES.Sciencetists, "sciencetists" },
            { GameEnum.GAME_CATEGORIES.Random, "random" },
        };

    public List<Word> GetByCategoryType(GameEnum.GAME_CATEGORIES categoryType)
    {
        List<string> words = _validWords[categoryType].ToList<string>();
        List<Word> result = new List<Word>();
        foreach(string word in words)
        {
            //result.Add(new Word(word,categoryType));
        }
        IEnumerator request = (GetByCategoryTypeRequest(categoryType));
        request.MoveNext();
        //var a = GetByCategoryTypeRequest(categoryType);
        //Task.WaitAll(a);
        //result = resultTask.Result;
        return result;
    }

    IEnumerator GetByCategoryTypeRequest(GameEnum.GAME_CATEGORIES categoryType)
    {
        string uri = $"{_host}api/Word/GetByCategoryType?categoryType={_enumToString[categoryType]}";
        UnityWebRequest www = UnityWebRequest.Get(uri);
        yield return www.Send();

        if (www.isNetworkError)
        {
            Debug.Log(www.error);
        }
        else
        {
            // Show results as text
            Debug.Log(www.downloadHandler.text);

            string responseBody = www.downloadHandler.text;
            var rawJson = JsonConvert.DeserializeObject<List<object>>(responseBody);

            for (int i = 0; i < rawJson.Count; i++)
            {
                var wordJson = rawJson[i] as JObject;
                string wordname = wordJson["name"].ToString();
                this.result.Add(new Word(wordname, categoryType));
            }
        }

    }

    /*public async Task<List<Word>> GetByCategoryTypeRequest(GameEnum.GAME_CATEGORIES categoryType)
    {
        string uri = $"{_host}api/Word/GetByCategoryType?categoryType={_enumToString[categoryType]}";
        List<Word> result = new();
        try
        {
            using (var client = new HttpClient())
            {

                HttpResponseMessage response = await client.GetAsync(uri);
                string responseBody = await response.Content.ReadAsStringAsync();

                var rawJson = JsonConvert.DeserializeObject<List<object>>(responseBody);

                for (int i = 0; i < rawJson.Count; i++)
                {
                    var wordJson = rawJson[i] as JObject;
                    string wordname = wordJson["name"].ToString();
                    result.Add(new Word(wordname, categoryType));
                }

            }
        } catch (Exception e)
        {
            Debug.Log("Exception! ->" + e.Message);
        }


        return result;}*/
}