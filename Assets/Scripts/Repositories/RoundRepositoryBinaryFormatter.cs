﻿using UnityEngine;

public class RoundRepositoryBinaryFormatter : IRoundRepository
{
    public Round GetById(int roundId, int gameId)
    {
        string path = Application.persistentDataPath + "/round" + roundId + "_" + gameId + ".bin";
        return RepositoryBinaryFormatter<Round>.ReadFromFilePath(path);
    }

    public void Save(Round round)
    {
        string path = Application.persistentDataPath + "/round" + round.RoundNumber + "_" + round.GameId + ".bin";
        RepositoryBinaryFormatter<Round>.Save(round, path);
    }
}

