﻿using UnityEngine;

public class TurnRepositoryBinaryFormatter : ITurnRepository
{
    public Turn GetById(int turnId, int roundId, int gameId)
    {
        string path = Application.persistentDataPath + "/turn" + turnId + "_" + roundId+ "_" + gameId + ".bin";
        return RepositoryBinaryFormatter<Turn>.ReadFromFilePath(path);
    }

    public void Save(Turn turn)
    {
        string path = Application.persistentDataPath + "/turn" + turn.Id + "_" + turn.RoundId + "_" + turn.GameId + ".bin";
        RepositoryBinaryFormatter<Turn>.Save(turn, path);
    }
}

