using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HomeView : MonoBehaviour, IHomeView
{
    Button button;
    public TMP_Text userInputText;
    private HomePresenter homePresenter;

    void Start()
    {
        button = GetComponentInChildren<Button>();
        homePresenter = new HomePresenter(this);
    }


    public Button GetButton()
    {
       return button;
    }

    public string GetInputName()
    {
       return userInputText.text;
    }
}
