using Assets.Scripts.Actions.Validators;
using System.Collections.Generic;
using UnityEngine;


public class PointCountPresenter { 
    private IPointCountView _view;
    private PointCountFacade pointCountFacade = new(0, new GameRepositoryBinaryFormatter());
   


    public PointCountPresenter(IPointCountView pointCountView)
    {
        this._view = pointCountView;
    }

    public void ShowResults()
    {
        CategoryWidget categories = _view.GetCategoryWidget();
        int points = pointCountFacade.ShowResult(categories);
        
        _view.SetPoints(points);
    }

    //Get current turn and fill the information with bot one
    public void GenerateBotTurn()
    {
        IGameRepository gameRepo = new GameRepositoryBinaryFormatter();
        Game actualGame = gameRepo.GetById(0);
        Round lastRound = actualGame.GetInProgressRound();
        Turn botTurn = lastRound.GetTurnInProgress();

        foreach(Word botWord in botTurn.userWords)
        {
            botWord.Name = Random.Range(0,10) > 5 ? "BotInput" : "Test"; //50% of correct word
        }
        lastRound.FinishActualTurn();
        actualGame.FinishActualRound();

        gameRepo.Save(actualGame);
        SceneSystem.LoadScene(SceneSystem.SceneIndex.Winner);
    }
}
