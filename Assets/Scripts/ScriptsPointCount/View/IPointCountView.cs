using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPointCountView
{
    CategoryWidget GetCategoryWidget();
    void SetPoints(int points);
}
