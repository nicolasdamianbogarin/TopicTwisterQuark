using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LoginController : MonoBehaviour
{
    public TMP_InputField Username;
    public TMP_InputField Password;
    public Button LoginButton;
    // Start is called before the first frame update
    void Start()
    {
        this.LoginButton.onClick.AddListener(OnLoginClick);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnLoginClick()
    {
        IPlayerRepository playerRepository = new PlayerRepositoryBinaryFormatter();
        if(playerRepository.Get(Username.text,Password.text) != null)
        {

        }
    }
}
