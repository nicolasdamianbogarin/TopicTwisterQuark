using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Widgets
{
    public class CategoryTab : MonoBehaviour, ICategoryTabWidget
    {
        private Image _backgroundImage;
        private TMP_Text _categoryText;
        private TMP_InputField _userInput;
        void Start() 
        {
            _categoryText = this.GetComponentInChildren<TMP_Text>();
            _userInput = this.GetComponentInChildren<TMP_InputField>();
            Image[] images = this.GetComponentsInChildren<Image>();
            _backgroundImage = null;
            for (int idx = 0; idx < images.Length && _backgroundImage == null; idx++)
            {
                if (images[idx].tag == "CategoryBackground")
                {
                    _backgroundImage = images[idx];
                }
            }
        }
        public void ChangeBackgroundColor(bool isGreen)
        {
            _backgroundImage.color = isGreen ? Color.green : Color.red;
        }

        public string GetTextCategory()
        {
            return _categoryText.text;
        }

        public string GetUserWord()
        {
            return _userInput.text;
        }

        public void SetImageCateogry(Sprite sprite)
        {
            Image[] images = this.GetComponentsInChildren<Image>();
            foreach (Image i in images)
            {
                if (i.tag == "CategoryIcon")
                {
                    i.sprite = sprite;
                }
            }
        }

        public void SetTextCategory(string name)
        {
            TMP_Text categoryText = this.GetComponentInChildren<TMP_Text>();
            categoryText.text = name;
        }

        public void SetUserWord(string userWord)
        {
            _userInput.text = userWord;
        }
    }

}