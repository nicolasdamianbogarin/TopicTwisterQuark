using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICategoryTabWidget
{
    void SetImageCateogry(Sprite sprite);
    void SetTextCategory(string name);
    void SetUserWord(string userWord);
    string GetTextCategory();
    string GetUserWord();
    void ChangeBackgroundColor(bool isGreen);
}
