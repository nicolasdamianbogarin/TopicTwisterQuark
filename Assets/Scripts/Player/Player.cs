using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Player
{
    [JsonProperty("name")]
    public string Name = "";
    [JsonProperty("email")]
    public string Email = "";

    public Player(string name)
    {
        Name = name;
    }

    public override bool Equals(object obj)
    {
        return obj is Player player &&
               Name == player.Name;
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(Name);
    }
}
