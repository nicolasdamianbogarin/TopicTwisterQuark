using Assets.Scripts.Utilities;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RandomCategory
{
    public List<Sprite> sprites;

    private List<CategoryDTO> _categoriesModel = new();
    private static List<CategoryDTO> _randomizedCategoriesModel = new();
    private static readonly string spritesPath = "SpriteCategories/";

    public RandomCategory(List<CategoryDTO> categories)
    {
        _categoriesModel = categories;
    }
    public RandomCategory()
    {
        _categoriesModel = CategoryDTOFactory.CreateAllCategories();
    }

    public void AddNewCategory(CategoryDTO newCategory)
    {
        if(!_categoriesModel.Contains(newCategory))
        {
            _categoriesModel.Add(newCategory);
        }
    }

    public CategoryDTO GetRandomCategory()
    {
        if (_randomizedCategoriesModel.Count == 0)
        {
            for(int i = 0; i < 5; i++) 
            {
                int randomIdx = Random.Range(0, _categoriesModel.Count);
                _randomizedCategoriesModel.Add(_categoriesModel[randomIdx]);
                _categoriesModel.RemoveAt(randomIdx);
            }
            _categoriesModel.AddRange(_randomizedCategoriesModel);
        }
        CategoryDTO randomCategory = _randomizedCategoriesModel.First();
        _randomizedCategoriesModel.RemoveAt(0);
        return randomCategory;
    }
}
