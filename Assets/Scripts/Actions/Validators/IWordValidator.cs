﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Actions.Validators
{
    public interface IWordValidator
    {
        public bool Validate(string word, string cateogry, char letter);
    }
}
