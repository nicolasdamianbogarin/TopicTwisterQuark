using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class EndGameController : MonoBehaviour
{
    public TMP_Text WinnerText;
    public Button GoHome;
    // Start is called before the first frame update
    void Start()
    {
        Game g = new GameRepositoryBinaryFormatter().GetById(0);
        string winnerName = g.GetWinner();
        WinnerText.text = winnerName;
        GoHome.onClick.AddListener(ReturnToHome);
    }

    private void ReturnToHome()
    {
        SceneSystem.LoadScene(SceneSystem.SceneIndex.Home);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
