using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameFacade
{

    private Game actualGame;
    private Turn actualTurn;
    private IGameRepository gameRepository;


    public GameFacade(int id, IGameRepository gameRepository)
    {
        this.gameRepository = gameRepository;
        actualGame = gameRepository.GetById(id);
        actualTurn = actualGame.GetInProgressRound().GetTurnInProgress();

    }

    public List<CategoryDTO> GetTurnCategories()
    {
        Turn actualTurn = actualGame.GetInProgressRound().GetTurnInProgress();
        List<CategoryDTO> result = new();
        for (int i = 0; i < 5; i++)
        {
            result.Add(actualTurn.categories[i].AsCategoryDTO());
        }
        return result;
    }

    public char GetTurnLetter()
    {
        return (actualTurn.Letter).letter;
    }

    public void EndTurn(Dictionary<string, string> categoryUserWordPairs)
    {
        
        List<CategoryWidgetDTO> result = new();
        foreach (var categoryUserWordPair in categoryUserWordPairs)
        {
            CategoryWidgetDTO category = new(categoryUserWordPair.Key, categoryUserWordPair.Value);
            result.Add(category);
        }

        for (int i = 0; i < actualTurn.userWords.Count; i++)
        {
            Category wordCategory = new Category(result[i].CategoryTitle);
            actualTurn.userWords[i] = new Word(result[i].UserInput, wordCategory.CategoryType);
        }

        gameRepository.Save(actualGame);
       
    }

    public string GetNamePlayer()
    {
       return actualGame.GetInProgressRound().GetTurnInProgress().playerTurn.Name;
    }
}
