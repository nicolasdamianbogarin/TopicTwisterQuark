﻿using Assets.Scripts.Actions.Validators;
using Codice.Client.Common.Connection;
using GameViewContract;
using System.Collections.Generic;
using UnityEngine;


namespace GamePresenterContract
{
    public class PartidaPresenter
    {
        private IPartidaView _view;
        private GameFacade gameFacade = new GameFacade(0, new GameRepositoryBinaryFormatter());

        public PartidaPresenter(IPartidaView view)
        {

            _view = view;
            _view.SubscribeToSendButton(EndTurn);
            _view.SubscribeToTimer(EndTurn);
            _view.SetPlayerName(gameFacade.GetNamePlayer());
        }

        public List<CategoryDTO> InitializeCategories()
        {
            return gameFacade.GetTurnCategories();
        }

        public char InitializeRandomLetter()
        {
            return gameFacade.GetTurnLetter();

        }

        public void EndTurn()
        {
            
            Dictionary<string, string> categoryUserWordPairs = _view.GetCategoryWordPairs();
            gameFacade.EndTurn( categoryUserWordPairs);
            _view.UpdateResults();
        }
    }
}
