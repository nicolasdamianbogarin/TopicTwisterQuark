﻿using System;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine.UI;

namespace GameViewContract
{
    public interface IPartidaView
    {
        void SubscribeToSendButton(UnityAction action);
        Dictionary<string,string> GetCategoryWordPairs();
        char GetWordLetter();
        void UpdateResults();
        void SubscribeToTimer(UnityAction validateWords);
        void SetPlayerName(string playerName);
    }
}
