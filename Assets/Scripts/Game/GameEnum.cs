﻿using System;

[Serializable]
public class GameEnum
{
    public enum GAME_STATE
    {
        IN_PROGRESS,
        FINISHED
    }
    public enum ROUND_STATE
    {
        WAITING,
        IN_PROGRESS,
        FINISHED
    }
    public enum TURN_STATE
    {
        WAITING,
        IN_PROGRESS,
        FINISHED
    }
    public enum GAME_CATEGORIES
    {
        Artists,
        Sports,
        Books,
        Countries,
        Movies,
        Videogames, 
        Sciencetists,
        Random
    }
}
