using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerDTO 
{
    [JsonProperty("name")]
    public string Name;
    [JsonProperty("email")]
    public string Email;


    public PlayerDTO(string name)
    {
        Name = name;
    }
}
