using System;
using UnityEngine;

[Serializable]
public class CategoryDTO
{
    public string Name;
    public Sprite Sprite;
}
