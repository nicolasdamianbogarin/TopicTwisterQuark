using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;

public class RoundWinnerController : MonoBehaviour
{
    public TMP_Text PlayerOneName;
    public TMP_Text PlayerTwoName;

    public TMP_Text PlayerOneScore;
    public TMP_Text PlayerTwoScore;

    public TMP_Text Winner;

    public Button NextRoundBtn;

    private Game actualGame;
    // Start is called before the first frame update
    void Start()
    {
        actualGame = new GameRepositoryBinaryFormatter().GetById(0);
        Round r = actualGame.GetLastFinishedRound();
        string playerOneName = r.Turns[0].playerTurn.Name;
        string playerTwoName = r.Turns[1].playerTurn.Name;
        int playerOnePoints = r.Turns[0].GetPoints();
        int playerTwoPoints = r.Turns[1].GetPoints();
        string winner = r.GetWinner();

        PlayerOneName.text = playerOneName;
        PlayerTwoName.text = playerTwoName;
        PlayerOneScore.text = playerOnePoints.ToString();
        PlayerTwoScore.text = playerTwoPoints.ToString();
        Winner.text = winner;

        NextRoundBtn.onClick.AddListener(NextRound);
    }

    private void NextRound()
    {
        if(actualGame.GameState == GameEnum.GAME_STATE.FINISHED)
        {
            SceneSystem.LoadScene(SceneSystem.SceneIndex.EndGame);
        }
        else
        {
            SceneSystem.LoadScene(SceneSystem.SceneIndex.Game);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
