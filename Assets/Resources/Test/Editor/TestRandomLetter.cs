using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class TestRandomLetter
{
    [Test]
    public void TestRandomLetterGeneratesALetter()
    {
        RandomLetter generator = new RandomLetter();
        char randomLetter = generator.GenerateRandomLetter().letter;
        Assert.IsNotNull(randomLetter);
    }
}
