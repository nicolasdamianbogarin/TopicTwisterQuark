using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UsersTableWidget : MonoBehaviour
{
    public GameObject PlayerOneScoreBoard;
    public GameObject PlayerTwoScoreBoard;
    // Start is called before the first frame update
    void Start()
    {
        Game g = new GameRepositoryBinaryFormatter().GetById(0);
        Image[] playerOneImages = PlayerOneScoreBoard.GetComponentsInChildren<Image>();
        Image[] playerTwoImages = PlayerTwoScoreBoard.GetComponentsInChildren<Image>();
        string playerOneName = g.Players[0].Name;
        string playerTwoName = g.Players[1].Name;
        Sprite[] sprites = Resources.LoadAll<Sprite>("2D Casual UI/Sprite");

        Sprite tieSprite = null;
        Sprite winSprite = null;
        Sprite loseSprite = null;
        for (int i = 0; i < sprites.Length && (tieSprite == null || winSprite == null || loseSprite == null); i++)
        {
            Sprite currentSprite = sprites[i];
            if (currentSprite.name == "GUI_61")
            {
                tieSprite = currentSprite;
            }
            else if (currentSprite.name == "GUI_66")
            {
                winSprite = currentSprite;
            }
            else if (currentSprite.name == "GUI_67")
            {
                loseSprite = currentSprite;
            }
        }


        for(int i = 0; i < 3; i++)
        {
            Round r = g.Rounds[i];
            string winnerName = r.GetWinner();
            if (winnerName == null)
            {
                //Round not played yet
                playerOneImages[i].sprite = null;
                playerOneImages[i].enabled = false;
                playerTwoImages[i].sprite = null;
                playerTwoImages[i].enabled = false;
            }
            else if (winnerName == "Tie")
            {
                playerOneImages[i].sprite = tieSprite;
                playerOneImages[i].rectTransform.sizeDelta = new Vector2(100, 40);
                playerTwoImages[i].sprite = tieSprite;
                playerTwoImages[i].rectTransform.sizeDelta = new Vector2(100, 40);

                playerOneImages[i].enabled = true;
                playerTwoImages[i].enabled = true;
            }
            else if(winnerName == playerOneName)
            {
                playerOneImages[i].sprite = winSprite;
                playerTwoImages[i].sprite = loseSprite;
                playerOneImages[i].enabled = true;
                playerTwoImages[i].enabled = true;
            }
            else if(winnerName==playerTwoName)
            {
                playerOneImages[i].sprite = loseSprite;
                playerTwoImages[i].sprite = winSprite;
                playerOneImages[i].enabled = true;
                playerTwoImages[i].enabled = true;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
